﻿#include <iostream>
#include <string>
using namespace std;


class Player
{
private:
	string NamePlayer;
	int ScorePlayer;
	
public:

	Player() : NamePlayer("Un"), ScorePlayer(0)
	{}

	Player(string _NamePlayer, int _ScorePlayer) : NamePlayer(_NamePlayer), ScorePlayer(_ScorePlayer)
	{}

	void Show()
	{
		cout << "\n" << NamePlayer << " : " << ScorePlayer;
	}

	int GetScorePlayer()
	{
		return ScorePlayer;
	}

	void SetScorePlayer(int NewScorePlayer)
	{
		ScorePlayer = NewScorePlayer;
	}

	string GetNamePlayer()
	{
		return NamePlayer;
	}

		void SetNamePlayer(string NewNamePlayer)
	{
		NamePlayer = NewNamePlayer;
	}

	void SetPlayer(string NewNamePlayer, int NewScorePlayer)
	{
		NamePlayer = NewNamePlayer;
		ScorePlayer = NewScorePlayer;
	}

};

	void SelectionSort(Player* base, int size)
	{
		int j;
		int i;
		Player  tmp;
		for (i = 0; i < size; i++)
		{
			j = i;
			for (int k = i; k < size; k++)
			{
				if (base[j].GetScorePlayer() > base[k].GetScorePlayer())
				{
					j = k;
				}
			}
			tmp = base[i];
			base[i] = base[j];
			base[j] = tmp;
		}
	}


	int main()
	{
		std::cout << "How many players do you want? \n";
		int size;
		cin >> size;

		string arrName;
		int arrScore;
		int i;
		Player* base = new Player[size];
		
		for (i = 0; i < size; i++)
		{
			cout << "Enter " << i + 1 << " name:";
			cin >> arrName;

			cout << "Enter " << arrName << "Score:";
			cin >> arrScore;

			base[i].SetPlayer(arrName, arrScore);
		}

		cout << "\n You enter: \n";
		for (i = 0; i < size; i++)
		{
			base[i].Show();
		}

		SelectionSort(base, size);
		cout << endl;
		for (i = 0; i < size; i++)
		{
			base[i].Show();
		}
		return 0;
	}